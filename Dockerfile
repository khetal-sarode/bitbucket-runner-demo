#stage 1
FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build --prod
#a
#stage 2
FROM nginx:alpine
COPY --from=node /app/dist/bitbucket-runner-demo /usr/share/nginx/html